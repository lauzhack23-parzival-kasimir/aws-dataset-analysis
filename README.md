# FixMyProduct
![logo](logo.png)

Projection submission to [LauzHack23](https://lauzhack.com/) by Kasimir Tanner and Parzival Nussbaum

FixMyProduct leverages the GPT 3.5 Turbo LLM to extract key insights for AWS customers selling products by extracting the key factors of customer satisfaction and/or dissatisfaction. 

## 
1. This is achieved by retrieving the highest ranked user reviews for a given product.
2. The reviews are summarized into bullet points, each bullet point contains a score from -1 (customer dissatisfaction) to 1 (customer satisfaction). 

## Interface

The user interacts with the pipeline through a [Django](https://www.djangoproject.com/) web app.

## Data

 The dataset may be found here: data/Amazon_Unlocked_Mobile.csv 

## Installation

- Install the conda packages listed in requirements.txt
- Run the code with `python app.py runserver`
- Provide OpenAI API Key in local_secrets/secrets.py in the OPENAI_KEY variable
- Provide the Django Secret key in local_secrets/secrets.py in the DJANGO_KEY variable

