from django.conf import settings
from django.http import HttpResponse
from django.shortcuts import render
from django.urls import path
from keys import DJANGO_KEY, OPENAI_KEY
from pathlib import Path
from django.conf.urls.static import static
import numpy as np
import pandas as pd
import json
import re
import time
from pipeline import LLMAnalyzer

settings.configure(
    DEBUG=True,
    ROOT_URLCONF=__name__,
    TEMPLATES=[
        {
            "BACKEND": "django.template.backends.django.DjangoTemplates",
            "DIRS": ["templates"],
        },
    ],
    SECRET_KEY=DJANGO_KEY,
    STATIC_URL='/static/',
    STATIC_ROOT=str(Path(__file__).resolve().parent) + '/static', 
)



data = pd.read_csv('data/Amazon_Unlocked_Mobile.csv')
data['Review Votes'] = data['Review Votes'].fillna(0).astype(float)
# fillna in Brand Name with "", ensure it is a string
data['Brand Name'] = data['Brand Name'].fillna("").astype(str)
# fillna in Price with 0
data['Price'] = data['Price'].fillna(0).astype(float)
# ensure Product name is a string
data['Product Name'] = data['Product Name'].astype(str)
# ensure Rating is a float
data['Rating'] = data['Rating'].astype(float)
# ensure Reviews is a string and fillna with ""
data['Reviews'] = data['Reviews'].fillna("").astype(str)

# only keep brands with more than 100 reviews
brands = data['Brand Name'].value_counts()
brands = brands[brands > 100]
brands = brands.index.values
data = data[data['Brand Name'].isin(brands)]



average_rating_per_product = data.groupby('Product Name')['Rating'].mean()
average_rating_per_product = average_rating_per_product.sort_values(ascending=False)

# get unique brands
brands = data['Brand Name'].unique()
# remove empty string
brands = brands[brands != '']
# get unique products by brand
products = {}
for brand in brands:
    brand_df = data[data['Brand Name'] == brand]
    # group by product name
    ps = brand_df.groupby('Product Name')['Product Name'].unique()
    # compute number of reviews per product
    ps = ps.apply(lambda x: len(x))
    # sort by number of reviews
    ps = ps.sort_values(ascending=False)
    # remove products with no reviews
    ps = ps[ps != 0]
    # get the product names
    ps = ps.index.values
    products[brand] = ps




def index_view(request):

    content = ""
    selected_category = ""
    selected_item = ""

    return render(request, 'index.html', {'brands': brands, 'products': products[brands[0]], 'content': content, 'selected_category': selected_category, 'selected_item': selected_item})



def get_products(request, brand):
    # return a http response with the products for the brand in json
    return HttpResponse(json.dumps(products[brand], cls=NumpyEncoder), content_type='application/json')

class Aspects(object):
    def __init__(self, summary, positive_aspects, negative_aspects, score = 0):
        self.summary = summary
        self.positive_aspects = positive_aspects
        self.negative_aspects = negative_aspects
        self.score = score

def escape_product(product):
    product = product.replace('&#39;', "'")
    # also for double quotes
    product = product.replace('&quot;', '"')
    # also for &
    product = product.replace('&amp;', '&')
    # escape /
    product = product.replace('_-_', '/')
    return product

def get_aspects(request, product):
    print("getting aspects")
    print(product)


    product = escape_product(product)

    analyzer = LLMAnalyzer()

    # wait 3 seconds before returning the reviews
    summary,score = analyzer.get_LLM_summary(product)
    suggestion = analyzer.get_LLM_suggestions(product)
    analyzer.save_cache()

    # round the score to 1 decimal place
    score = round(score, 1)
    # print the score
    print("score")
    print(score)
    

    print("summary")
    print(summary)
    print("end summary")

    # put the summary on one line
    summary = summary.replace('\n', ' ')

    response = Aspects(summary, positive_aspects=[], negative_aspects=[])

    response.summary = summary
    response.score = score

    # write a regex to get all bullet points for the positive aspects
    positive_aspects = re.findall(r'Positive Aspects: (.*)Negative Aspects:', summary)

    print(positive_aspects)

    # write a regex to get all bullet points for the negative aspects
    negative_aspects = re.findall(r'Negative Aspects: (.*)', summary)

    print(negative_aspects)
    try:

        response.positive_aspects = positive_aspects[0].split('+')
    except:
        response.positive_aspects = []
    try:
        response.negative_aspects = negative_aspects[0].split('-')
    except:
        response.negative_aspects = []

    # strip whitespace
    response.positive_aspects = [x.strip() for x in response.positive_aspects]
    response.negative_aspects = [x.strip() for x in response.negative_aspects]

    # remove empty strings
    response.positive_aspects = [x for x in response.positive_aspects if x != '']
    response.negative_aspects = [x for x in response.negative_aspects if x != '']

    print(response.positive_aspects)
    print(response.negative_aspects)

    response.suggestion = suggestion
    print("suggestion")
    print(response.suggestion)
    print("end suggestion")

    # if suggestion contains I'm sorry, then set suggestion to empty string
    if "I'm sorry" in response.suggestion:
        response.suggestion = ""
    # if "But I'll need a bit more" in response.suggestion:
    if "But I'll need a bit more" in response.suggestion:
        response.suggestion = ""

    # write some regex to split the text into sentences by separating at digit.
    response.suggestions = re.split(r'(\d\.)', response.suggestion)
    # remove empty strings
    response.suggestions = [x for x in response.suggestions if x != '']
    # keep every other element other element
    response.suggestions = response.suggestions[1::2]

    

    # return a http response with the reviews for the product in json
    return HttpResponse(json.dumps(response, cls=NumpyEncoder), content_type='application/json')

def get_bar_data(request, product):

    print("getting bar data")
    print(product)
    # unescape the product name

    product = escape_product(product)

    product_reviews = data[data['Product Name'] == product]

    # print len of product reviews
    print(len(product_reviews))

    # count the number of reviews per rating
    vc = product_reviews['Rating'].value_counts()
    # order the number of reviews per rating
    vc = vc.sort_index()
    print(vc)
    # convert to a numpy array
    vc = vc.values

    return HttpResponse(json.dumps(vc, cls=NumpyEncoder), content_type='application/json')

def dashboard(request):
    brand = request.POST.get('brand')
    product = request.POST.get('product')

    # create an object to store the data
    class Response(object):
        pass

    response = Response()

    response.brand = brand
    response.product = product

    

    # count the number of reviews
    filtered_product = data[data['Product Name'] == product]
    response.num_reviews = len(filtered_product)

    # get the average rating
    response.avg_rating = filtered_product['Rating'].mean()
    if np.isnan(response.avg_rating):
        response.avg_rating = 1
    # round to 1 decimal places
    response.avg_rating = round(response.avg_rating, 1)

    # brand average rating
    filtered_brand = data[data['Brand Name'] == brand]
    response.brand_avg_rating = filtered_brand['Rating'].mean()
    # replace nan with 1
    if np.isnan(response.brand_avg_rating):
        response.brand_avg_rating = 1


    response.int_avg_rating = int(response.avg_rating)
    response.checked = []
    for i in range(5):
        if i < response.int_avg_rating:
            response.checked.append(True)
        else:
            response.checked.append(False)

    # round to 1 decimal places
    response.brand_avg_rating = round(response.brand_avg_rating, 1)

    index = average_rating_per_product.index.get_loc(product)
    response.top_n = 100 - (index / len(average_rating_per_product)) * 100
    # round to 0 decimal places
    response.top_n = round(response.top_n, 0)
    

    filtered_product = filtered_product.sort_values(by=['Review Votes'], ascending=False)
    filtered_product = filtered_product.drop_duplicates(subset=['Reviews'], keep='first')
    top_5_reviews = filtered_product.head(5)
    reviews = top_5_reviews['Reviews'].tolist()
    review_votes = top_5_reviews['Review Votes'].tolist()
    idx = np.linspace(0, len(reviews) - 1, len(reviews), dtype=int)
    response.reviews = []
    for i in range(len(idx)):
        review = Response()
        review.review = reviews[idx[i]]
        review.review_votes = review_votes[idx[i]]
        review.idx = idx[i]
        response.reviews.append(review)

    # escape / in product name
    product = product.replace('/', '_-_')
    response.product = product

    return render(request, 'dashboard.html', {'data': response})

# Write a numpy json encoder
class NumpyEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.ndarray):
            return obj.tolist()
        if isinstance(obj, Aspects):
            return obj.__dict__
        return json.JSONEncoder.default(self, obj)
    

urlpatterns = [
    path("", index_view),
    path("/", index_view),
    path('get_products/<brand>', get_products),
    path('get_aspects/<product>', get_aspects),
    path('dashboard/', dashboard, name='dashboard'),
    path('get_bar_data/<product>', get_bar_data)
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

if __name__ == "__main__":
    from django.core.management import execute_from_command_line

    execute_from_command_line()
