import pandas as pd
import concurrent.futures
import re
from local_secrets.secrets import OPENAI_KEY
from openai import OpenAI
from prompts import SUMMARIZE_SYS_PROMPT
from prompts import SYNTHESIZE_SYS_PROMPT
from prompts import IMPROVEMENT_SYS_PROMPT

class LLMAnalyzer:
    def __init__(self, data_path='data/Amazon_Unlocked_Mobile.csv', cache_path='cache.csv'):
        self.client = OpenAI(api_key=OPENAI_KEY)
        self.data = pd.read_csv(data_path)
        # Read the cache from disk if it exists
        try:
            self.cache = pd.read_csv(cache_path)
        except FileNotFoundError:
            self.cache = pd.DataFrame(columns=['Product Name', 'Summary', 'Score'])

        self._cache_path = cache_path
        self.sys_prompt_summarize = SUMMARIZE_SYS_PROMPT
        self.sys_prompt_synthesize = SYNTHESIZE_SYS_PROMPT
        self._preprocessed = False
        
        self._MAX_WORKERS = 20
        self._BEST_N_REVIEWS = 20
        
        
    def preprocess_data(self):
        self.data['Review Votes'] = self.data['Review Votes'].fillna(0).astype(float)
        self.data['Brand Name'] = self.data['Brand Name'].fillna("").astype(str)
        self.data['Price'] = self.data['Price'].fillna(0).astype(float)
        self.data['Product Name'] = self.data['Product Name'].astype(str)
        self.data['Rating'] = self.data['Rating'].astype(float)
        self.data['Reviews'] = self.data['Reviews'].fillna("").astype(str)

        self._preprocessed = True

    def _summarize_review(self, review):
        print(f"Summarizing review: {review}")
        try:
            completion = self.client.chat.completions.create(
                model="gpt-3.5-turbo",
                messages=[
                    {"role": "system", "content": self.sys_prompt_summarize},
                    {"role": "user", "content": review},
                ],
                temperature=0.05,
                max_tokens=40
            )
            summary = completion.choices[0].message.content
            print(f"Summary: {summary}")
            return summary
        except Exception as e:
            print(f"Error occurred during review summarization: {str(e)}")
            return ""

    def _extract_main_points(self, summaries):
        try:
            completion = self.client.chat.completions.create(
                model="gpt-3.5-turbo-1106",
                messages=[
                    {"role": "system", "content": self.sys_prompt_synthesize},
                    {"role": "user", "content": "Here are the summaries of the reviews: \n".join(summaries)},
                ],
                temperature=0.05,
                max_tokens=200
            )
            return completion.choices[0].message.content
        except Exception as e:
            print(f"Error occurred during main points extraction: {str(e)}")
            return ""
        
    def _get_top_reviews(self, product_name):
        print(f"Getting top reviews for {product_name}")
        # print length of data
        print(f"Length of data: {len(self.data)}")
        filtered_product = self.data[self.data['Product Name'] == product_name]
        print(f"Length of filtered product: {len(filtered_product)}")
        filtered_product = filtered_product.sort_values(by=['Review Votes'], ascending=False)
        print(f"Length of filtered product: {len(filtered_product)}")
        filtered_product = filtered_product.drop_duplicates(subset=['Reviews'], keep='first')
        print(f"Length of filtered product: {len(filtered_product)}")
        top_reviews = filtered_product.head(self._BEST_N_REVIEWS)
        print(f"Length of top reviews: {len(top_reviews)}")
        return top_reviews
    
    def get_LLM_summary(self, product_name, recompute=False):

        # remove the quotes from the product name
        product_name = product_name.replace('"', '')

        if not self._preprocessed:
            self.preprocess_data()
        # Check if the product is in the cache
        if product_name in self.cache['Product Name'].tolist() and not recompute:
            print("Cache hit!")
            cached_main_points = self.cache[self.cache['Product Name'] == product_name]['Summary'].tolist()[0]
            cached_score = self.cache[self.cache['Product Name'] == product_name]['Score'].tolist()[0]
            return cached_main_points, cached_score
        

        print("Cache miss!")
        print(f"Analyzing product: {product_name}")

        top_reviews = self._get_top_reviews(product_name)

        print("Top reviews:")
        print(top_reviews)
        print("End of top reviews")

        reviews = top_reviews['Reviews'].tolist()

        print("Reviews:")
        print(reviews)
        print("End of reviews")

        summaries = []
        with concurrent.futures.ThreadPoolExecutor(max_workers=self._MAX_WORKERS) as executor:
            try:
                summaries = list(executor.map(lambda review: self._summarize_review(review), reviews, timeout=30))
                print("Summaries:")
                print(summaries)
                print("End of summaries")
            except TimeoutError:
                print("Timed out!")
                  


        summaries = ["###\n" + summary + "\n###" for summary in summaries]

        print("Summaries:")
        print(summaries)
        print("End of summaries")

        main_feedback_points = self._extract_main_points(summaries)

        # Use regex to match any float between -1 and 1 lcated between || and ||
        score = 0 
        for summary in summaries:
            rating_strings = re.findall(r'\|\| ?(-?1|-?\d\.\d) ?\|\|', summary)
            score += sum([float(rating) for rating in rating_strings])

        # Normalize the score
        if len(summaries) > 0:
            score /= len(summaries)
        
        # Cache the result or overwrite the old entry
        if product_name in self.cache['Product Name'].tolist():
            self.cache.loc[self.cache['Product Name'] == product_name, 'Summary'] = main_feedback_points
            self.cache.loc[self.cache['Product Name'] == product_name, 'Score'] = score
        else:
            self.cache = pd.concat([self.cache, pd.DataFrame([[product_name, main_feedback_points, score]], columns=['Product Name', 'Summary', 'Score'])])
            
        return main_feedback_points, score

    def save_cache(self):
        self.cache.to_csv(self._cache_path, index=False)
    
    def get_LLM_suggestions(self, product_name):

        # remove the quotes from the product name
        product_name = product_name.replace('"', '')


        top_reviews = self._get_top_reviews(product_name)['Reviews'].tolist()
        summaries = []
        with concurrent.futures.ThreadPoolExecutor(max_workers=self._MAX_WORKERS) as executor:
            try:
                summaries = list(executor.map(lambda review: self._summarize_review(review), top_reviews, timeout=30))
            except TimeoutError:
                print("Timed out!")
        summaries = ["###\n" + summary + "\n###" for summary in summaries]

        try:
            completion = self.client.chat.completions.create(
                model="gpt-3.5-turbo-1106",
                messages=[
                    {"role": "system", "content": IMPROVEMENT_SYS_PROMPT},
                    {"role": "user", "content": "Here are the summaries of the reviews: \n".join(summaries)},
                ],
                temperature=0.05,
                max_tokens=200
            )
            return completion.choices[0].message.content
        except Exception as e:
            print(f"Error occurred during main points extraction: {str(e)}")
            return ""
        
        

if __name__ == '__main__':
    analyzer = LLMAnalyzer()
    try:
        #print(analyzer.get_LLM_summary('Samsung Galaxy S Duos II S7582 DUAL SIM Factory Unlocked International Version - Black'))
       # print(analyzer.get_LLM_summary('"CLEAR CLEAN ESN" Sprint EPIC 4G Galaxy SPH-D700FRONT CAMERAANDROIDSLIDERQWERTY KEYBOARD*TOUCH SCREEN - Samsung"'))
        print(analyzer.get_LLM_suggestions('Samsung Galaxy S Duos II S7582 DUAL SIM Factory Unlocked International Version - Black'))
        analyzer.save_cache()
    except Exception as e:
        print(f"Error occurred during LLM analysis: {str(e)}")
