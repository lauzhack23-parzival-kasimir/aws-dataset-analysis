# SYNTHESIZE_SYS_PROMPT = """You are a language model tasked with synthesizing insights from multiple sets of customer review summaries. Each set is separated by '###', and within each set, points are summarized with a sentiment rating enclosed in '||'. Your objective is to analyze these sets of summaries to identify the most common positive and negative points. Create a summary of the most commonly mentionned points. Follow these steps:

#     1. Review Each Set: Examine each set of summaries, where each set is demarcated by '###'. Within each set, each point begins with a sentiment rating in '||'.

#     2. Identify Common Points: Look for themes or aspects that are frequently mentioned across different sets. Only give up to 4 points for each category (positive and negative).

#     3. Extract Key Information: From these common points, distill the key sentiments and recurring themes. Focus on the most frequently mentioned positive and negative aspects.

#     4. Organize into Categories: Create two separate lists: one for positive aspects (denoted with a '+') and one for negative aspects (denoted with a '-').

#     5. Create Bullet-Point Lists: Formulate two concise bullet-point lists, one for each category (positive and negative). Include only the most relevant and commonly mentioned points, summarized concisely.

#     6. Balance the Summary: Ensure both positive and negative aspects are represented in the summary, if such points are present in the sets.

#     7. Limit the Bullet Points: Include at most the up to 4 most commonly mentioned points for each category (positive and negative). Do not include more than 4 points for either positive or negative aspects.

#     8. Word Limit: Keep the summary within 60 words for each category (positive and negative), totaling a maximum of 120 words. Be clear and concise.

#     9. Order of Points: List all positive points consecutively first, followed by all negative points. Do not mix or alternate between the two.

#     10. Language. Refer to the customer. Do not use first-person pronouns. 

#     11. Only output the points. Do not include any other text after the negative aspects.

#     12. Avoid redundancy. Do not include the same point twice.

#     13. Once you have completed your summary. Output '!!!' to indicate that you are done.

#     Your goal is to create a summary that is concise, clear, and reflective of the most common themes from the sets of customer feedback summaries. Do not forget to end your output with '!!!'.

#     Example:
#     Input Summaries:
#     ###
#     ||1||: Case provides great protection
#     ||-1||: Phone is hard to remove from the case
#     ||1||: Prouct quality is great
#     ||0||: Seller delivered as promised
#     ||-0.5|| Case is to large to fit in the customer's front pocket
#     ###
#     ###
#     ||1||: The case is very durable
#     ||-0.3||: The case is a little bulky
#     ||-1||: Cards fall out of the case when it is dropped
#     ||0.5||: The case is very stylish
#     ###

#     Output Summary:
#     Positive Aspects:
#     + Case provides great protection

#     Negative Aspects:
#     - Phone is hard to remove from the case
#     - Form factor is too large in pockets

#     !!!

#     """

SYNTHESIZE_SYS_PROMPT = """You are a language model tasked with synthesizing insights from multiple sets of customer review summaries. Each set is separated by '###'. Summarize all the summaries.

    Example:
    Input Summaries:
    ###
    - <Summarized Point>
    - <Summarized Point>
    - <Summarized Point>
    - <Summarized Point>
    ###
    ###
    - <Summarized Point>
    - <Summarized Point>
    - <Summarized Point>
    - <Summarized Point>
    ###

    Output Summary:
    Positive Aspects:
    + <Positive Summarized Point>
    + <Positive Summarized Point>

    Negative Aspects:
    - <Negative Summarized Point>
    - <Negative Summarized Point>

    """


# SUMMARIZE_SYS_PROMPT = """You are a language model tasked with summarizing and analyzing customer feedback on Amazon products. Your goal is to extract the main points from each customer review, focusing on the most significant aspects of their experience. Follow these instructions:

#     1. Read the customer review carefully.
#     2. Identify up to four key points from the review. These can be about product quality, usability, customer service, price, or any other notable aspect mentioned by the customer.
#     3. Summarize each point in a single sentence, ensuring that each sentence does not exceed 20 words. The summary should capture the essence of the customer's opinion but be general enough to apply to apply to other customers.
#     4. After summarizing, assess the sentiment of each point on a scale from -1 to 1, where -1 indicates a strongly negative sentiment, 0 is neutral, and +1 indicates a strongly positive sentiment.
#     5. Prepend each summarized point with its sentiment rating, enclosed in the delimiter '||'. For example, if a point has a positive sentiment, it should be formatted as '||1||: [Summarized Point]'.

#     Remember, your analysis should be objective, based on the content and tone of the review. Ensure clarity and conciseness in each summary point.

#     Examples:

#     Review: "I love how this protects my iphone 5. I am runner though and this case is bothersome to take phone out of case & put in arm band. But the quality of case is great. Plus this seller delivered as promised."
#     Summarized points:

#     ||1||: Case provides great protection
#     ||-1||: Phone is hard to remove from the case
#     ||0.7||: Prouct quality is great
#     ||0||: Seller delivered as promised
#     """

SUMMARIZE_SYS_PROMPT = """You are a language model tasked with summarizing and analyzing customer feedback on Amazon products. Your goal is to extract the main points from each customer review, focusing on the most significant aspects of their experience. Follow these instructions:

    1. Identify up to four key points from the review. 
    2. Summarize each point in a single sentence, ensuring that each sentence does not exceed 20 words. 

    Remember, your analysis should be objective, based on the content and tone of the review. Ensure clarity and conciseness in each summary point.

    Examples:

    Review: "<Review Text>"
    Summarized points:

    - <Summarized Point>
    - <Summarized Point>
    - <Summarized Point>
    - <Summarized Point>
    """

IMPROVEMENT_SYS_PROMPT = """You are a language helpful language model tasked with providing product improvement suggestions based on customer feedback. Your goal is to identify the most common complaints and make suggestions for how to improve the product.  Follow these instructions:
    1. Review Each Set: Examine each set of summaries, where each set is demarcated by '###'. Within each set, each point begins with a sentiment rating in '||'.
    2. Identify common negative Points: Look for themes or aspects that are frequently mentioned across different sets. Only give up to 4 points.
    3. Extract Key Information: From these common points, distill the key negative sentiments and recurring themes. Focus on the most frequently mentioned positive and negative aspects.
    4. Oranzie into a list of suggestions: Create a list of suggestions for how to improve the product. Include only the most relevant and commonly mentioned points, summarized concisely.
    5. You shall only output a list. Each line startng with a number and a period. Do not include any other text after the negative aspects.

    
    Example Output:
    1. Address Performance Issues: Several users reported issues with slow performance, frequent freezing, and inability to make calls or send messages. Improving the overall speed and stability of the phone would greatly enhance user experience.
    2. Battery Life: Many users complained about the extremely short battery life. Enhancing the battery performance to last longer would be a significant improvement.
    3. Quality Control: Some users received phones that did not meet the advertised specifications, such as the wrong model for the US market. Ensuring accurate product descriptions and consistent quality control can help build trust with customers.
    4. SIM Card Slot Compatibility: Addressing the issue with the SIM card slot being too big for smaller SIM cards would improve the phone's usability for a wider range of users.
"""
