// /* globals Chart:false, feather:false */

// (function () {
//   'use strict'

//   feather.replace({ 'aria-hidden': 'true' })



//   // Graphs
//   var ctx = document.getElementById('myChart');
//   // eslint-disable-next-line no-unused-vars
//   var myChart = new Chart(ctx, {
//     type: 'bar',
//     data: {
//       labels: [
//         '1 Star',
//         '2 Star',
//         '3 Star',
//         '4 Star',
//         '5 Star'
//       ],
//       datasets: [{
//         data: [
//           15339,
//           21345,
//           18483,
//           24003,
//           23489,
//         ],
//         lineTension: 0,
//         borderColor: '#3081D0',
//         backgroundColor: '#6DB9EF',
//         borderWidth: 2,
//         borderRadius: 5,
//         label: 'Ratings'
//       }]
//     },
//     options: {
//       scales: {
//         yAxes: [{
//           ticks: {
//             beginAtZero: false
//           }
//         }]
//       },
//       legend: {
//         display: true
//       },
//       responsive: true
//     }
//   })
// })()
